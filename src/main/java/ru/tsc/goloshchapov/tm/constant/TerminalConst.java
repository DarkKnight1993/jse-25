package ru.tsc.goloshchapov.tm.constant;

public class TerminalConst {

    public static final String ABOUT = "about";

    public static final String VERSION = "version";

    public static final String HELP = "help";

    public static final String EXIT = "exit";

    public static final String INFO = "info";

    public static final String COMMANDS = "commands";

    public static final String ARGUMENTS = "arguments";

    public static final String PROJECT_LIST = "project-list";

    public static final String PROJECT_CLEAR = "project-clear";

    public static final String PROJECT_CREATE = "project-create";

    public static final String PROJECT_SHOW_BY_ID = "project-show-by-id";

    public static final String PROJECT_SHOW_BY_INDEX = "project-show-by-index";

    public static final String PROJECT_SHOW_BY_NAME = "project-show-by-name";

    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    public static final String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    public static final String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    public static final String PROJECT_START_BY_ID = "project-start-by-id";

    public static final String PROJECT_START_BY_INDEX = "project-start-by-index";

    public static final String PROJECT_START_BY_NAME = "project-start-by-name";

    public static final String PROJECT_FINISH_BY_ID = "project-finish-by-id";

    public static final String PROJECT_FINISH_BY_INDEX = "project-finish-by-index";

    public static final String PROJECT_FINISH_BY_NAME = "project-finish-by-name";

    public static final String PROJECT_CHANGE_STATUS_BY_ID = "project-change-status-by-id";

    public static final String PROJECT_CHANGE_STATUS_BY_INDEX = "project-change-status-by-index";

    public static final String PROJECT_CHANGE_STATUS_BY_NAME = "project-change-status-by-name";

    public static final String TASK_LIST = "task-list";

    public static final String TASK_CLEAR = "task-clear";

    public static final String TASK_CREATE = "task-create";

    public static final String TASK_SHOW_BY_ID = "task-show-by-id";

    public static final String TASK_SHOW_BY_INDEX = "task-show-by-index";

    public static final String TASK_SHOW_BY_NAME = "task-show-by-name";

    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";

    public static final String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";

    public static final String TASK_UPDATE_BY_ID = "task-update-by-id";

    public static final String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    public static final String TASK_START_BY_ID = "task-start-by-id";

    public static final String TASK_START_BY_INDEX = "task-start-by-index";

    public static final String TASK_START_BY_NAME = "task-start-by-name";

    public static final String TASK_FINISH_BY_ID = "task-finish-by-id";

    public static final String TASK_FINISH_BY_INDEX = "task-finish-by-index";

    public static final String TASK_FINISH_BY_NAME = "task-finish-by-name";

    public static final String TASK_CHANGE_STATUS_BY_ID = "task-change-status-by-id";

    public static final String TASK_CHANGE_STATUS_BY_INDEX = "task-change-status-by-index";

    public static final String TASK_CHANGE_STATUS_BY_NAME = "task-change-status-by-name";

    public static final String PROJECT_TASK_BIND_BY_ID = "project-task-bind-by-id";

    public static final String PROJECT_TASK_UNBIND_BY_ID = "project-task-unbind-by-id";

    public static final String PROJECT_TASK_SHOW_BY_ID = "project-task-show-by-id";

    public static final String LOGIN = "login";

    public static final String LOGOUT = "logout";

    public static final String REGISTRY = "registry";

    public static final String CHANGE_PASSWORD = "change-password";

    public static final String VIEW_PROFILE = "view-profile";

    public static final String UPDATE_PROFILE = "update-profile";

}
