package ru.tsc.goloshchapov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.goloshchapov.tm.command.AbstractSystemCommand;

public final class AboutShowCommand extends AbstractSystemCommand {
    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: " + serviceLocator.getPropertyService().getDeveloperName());
        System.out.println("E-MAIL: " + serviceLocator.getPropertyService().getDeveloperEmail());
    }

}
