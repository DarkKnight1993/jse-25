package ru.tsc.goloshchapov.tm.comparator;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.command.AbstractCommand;

import java.util.Comparator;

public class ComparatorCommandByName implements Comparator<Class<? extends AbstractCommand>> {

    @NotNull
    private static final ComparatorCommandByName INSTANCE = new ComparatorCommandByName();

    private ComparatorCommandByName() {
    }

    @NotNull
    public static ComparatorCommandByName getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable Class<? extends AbstractCommand> o1, @Nullable Class<? extends AbstractCommand> o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getSuperclass().getName().equals(o2.getSuperclass().getName()))
            return o1.getName().compareTo(o2.getName());
        return o1.getSuperclass().getName().compareTo(o2.getSuperclass().getName());
    }
}
