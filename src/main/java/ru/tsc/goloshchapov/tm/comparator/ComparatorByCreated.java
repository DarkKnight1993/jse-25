package ru.tsc.goloshchapov.tm.comparator;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.api.entity.IHasCreated;

import java.util.Comparator;

public final class ComparatorByCreated implements Comparator<IHasCreated> {

    @NotNull
    private static final ComparatorByCreated INSTANCE = new ComparatorByCreated();

    private ComparatorByCreated() {
    }

    @NotNull
    public static ComparatorByCreated getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasCreated o1, @Nullable final IHasCreated o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getCreated().compareTo(o2.getCreated());
    }

}
