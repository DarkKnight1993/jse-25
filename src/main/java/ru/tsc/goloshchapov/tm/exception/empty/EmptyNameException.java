package ru.tsc.goloshchapov.tm.exception.empty;

import ru.tsc.goloshchapov.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Exception! Name is empty!");
    }

}
