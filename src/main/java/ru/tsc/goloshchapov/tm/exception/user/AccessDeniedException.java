package ru.tsc.goloshchapov.tm.exception.user;

import ru.tsc.goloshchapov.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Exception! Access is denied!");
    }

}
