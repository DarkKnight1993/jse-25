package ru.tsc.goloshchapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.api.IRepository;
import ru.tsc.goloshchapov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> entities = new ArrayList<>();

    @Override
    public void add(@NotNull E entity) {
        entities.add(entity);
    }

    @Override
    public void remove(@NotNull final E entity) {
        entities.remove(entity);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return entities.stream().anyMatch(e -> id.equals(e.getId()));
    }

    @Override
    public boolean existsByIndex(@NotNull final Integer index) {
        return entities.stream()
                .skip(index)
                .findFirst()
                .orElse(null) != null;
    }

    @Nullable
    @Override
    public List<E> findAll() {
        return entities;
    }

    @Nullable
    @Override
    public List<E> findAll(@NotNull final Comparator<E> comparator) {
        return entities.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public E findById(@NotNull final String id) {
        return entities.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public E findByIndex(@NotNull final Integer index) {
        return entities.stream()
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findById(id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final Integer index) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findByIndex(index));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public void clear() {
        entities.clear();
    }

}
