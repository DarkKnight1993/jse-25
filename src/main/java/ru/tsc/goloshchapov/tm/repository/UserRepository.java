package ru.tsc.goloshchapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.api.repository.IUserRepository;
import ru.tsc.goloshchapov.tm.model.User;

import java.util.Optional;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return entities.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return entities.stream()
                .filter(e -> email.equals(e.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public User removeUser(@NotNull final User user) {
        entities.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        @NotNull final Optional<User> user = Optional.ofNullable(findByLogin(login));
        user.ifPresent(this::removeUser);
        return user.orElse(null);
    }

}
