package ru.tsc.goloshchapov.tm.api.entity;

public interface IWBS extends IHasCreated, IHasName, IHasStartDate, IHasStatus {
}
