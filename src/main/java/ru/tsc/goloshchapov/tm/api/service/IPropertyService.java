package ru.tsc.goloshchapov.tm.api.service;

import ru.tsc.goloshchapov.tm.api.setting.ISaltSettings;

public interface IPropertyService extends ISaltSettings {

    String getApplicationVersion();

    String getDeveloperName();

    String getDeveloperEmail();

}
